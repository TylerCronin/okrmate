import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddKeyResultButtonComponent } from './add-key-result-button.component';

describe('AddKeyResultButtonComponent', () => {
  let component: AddKeyResultButtonComponent;
  let fixture: ComponentFixture<AddKeyResultButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKeyResultButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddKeyResultButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
