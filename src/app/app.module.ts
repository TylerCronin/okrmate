import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleComponent } from './title/title.component';
import { SaveButtonComponent } from './save-button/save-button.component';
import { ObjectiveComponent } from './objective/objective.component';
import { AddKeyResultButtonComponent } from './add-key-result-button/add-key-result-button.component';
import { KeyResultComponent } from './key-result/key-result.component';

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    SaveButtonComponent,
    ObjectiveComponent,
    AddKeyResultButtonComponent,
    KeyResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
